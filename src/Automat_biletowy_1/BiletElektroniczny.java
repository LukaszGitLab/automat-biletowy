package Automat_biletowy_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class BiletElektroniczny extends JFrame {

	MojPanel panelA;
	MojPanel2 panelB;
	JButton button1;
	JButton button2;
	Date d = new Date();
	String[] tabDzienTyg = {"Mon", "Tue", "Wen", "Thu", "Fri", "Sat", "Sun"};
	JButton[] tabDni = new JButton[jakiMiesiac()];

	int iloscDniWMiesiacu;

	public static void main(String[] args) {

		new BiletElektroniczny();

		// new Kalendarz();

		 Date d = new Date();
		 System.out.println("Dzisiejsza data: " + d);
//		 System.out.println("Date " + d.getDate());
//		 System.out.println("Day " + d.getDay());
//		 System.out.println("Hours " + d.getHours());
//		 System.out.println("Minutes " + d.getMinutes());
//		 System.out.println("Month " + d.getMonth());
//		 System.out.println("Second " + d.getSeconds());
//		 System.out.println("Time " + d.getTime());
//		 System.out.println("TimezoneOffset " + d.getTimezoneOffset());
//		 System.out.println(1900 + d.getYear());
		 /*
		  *Thu Jan 18 18:06:34 CET 2018
			Date 18
			Day 4
			Hours 18
			Minutes 6
			Month 0
			Second 34
			Time 1516295194484
			TimezoneOffset -60
			2018
		  * */

		String[] elementyDaty = new String("" +d).split(" ");
		for(int i = 0; i < elementyDaty.length; i++){
			System.out.println("elementyDaty: " + i + ", " + elementyDaty[i]);
		}
		
		System.out.println("Koniec");

	}

	public BiletElektroniczny() {

		setTitle("Bilet Elektroniczny");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panelA = new MojPanel();
		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		panelB = new MojPanel2();
		panelB.setLayout( new GridLayout(0,7,7,7));
		
		button1 = new JButton("" + d);
		panelA.add(button1);

		button2 = new JButton("Od�wie�");
		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				panelA.revalidate();
				panelA.repaint();

			}
		});
		panelA.add(button2);

		for (int i = 0; i < tabDzienTyg.length; i++){
			JLabel labelNazwaDniTyg = new JLabel("" + tabDzienTyg[i]);
			labelNazwaDniTyg.setHorizontalAlignment(JLabel.CENTER);
			labelNazwaDniTyg.setVerticalAlignment(JLabel.CENTER);
			panelB.add(labelNazwaDniTyg);
		}
		for (int i = 0; i < tabDni.length; i++) {
			tabDni[i] = new JButton("" + (i + 1));
			tabDni[i].setName("" + i);
			tabDni[i].addMouseListener(new MouseListener() {
				@Override
				public void mouseReleased(MouseEvent e) {
				}

				@Override
				public void mousePressed(MouseEvent e) {
				}

				@Override
				public void mouseExited(MouseEvent e) {
					JButton b = (JButton) e.getSource();
					tabDni[Integer.parseInt(b.getName())].setBackground(null);
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					JButton b = (JButton) e.getSource();
					tabDni[Integer.parseInt(b.getName())]
							.setBackground(Color.RED);
				}

				@Override
				public void mouseClicked(MouseEvent e) {
				}
			});
			tabDni[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JButton b = (JButton) e.getSource();
					System.out.println("Klikni�to " + b.getName());
				}
			});
			panelB.add(tabDni[i]);
		} // for

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().add(BorderLayout.NORTH, panelA);
		getContentPane().add(BorderLayout.EAST, panelB);
//		add(panelA);
//		add(panelB);
//		setSize(1000, 1000);
		pack();
		setLocation(800, 500);
		setLocationRelativeTo(null);
		setVisible(true);
	}

///////////////////////////////////////////////////////////////////////////////////
	class MojPanel extends JPanel {
		public void paint(Graphics g) {
			super.paint(g);

			d = new Date();
			button1.setText("" + d);

		}
	}
///////////////////////////////////////////////////////////////////////////////////
	class MojPanel2 extends JPanel{
		public void paint(Graphics g) {
			super.paint(g);

		}
	}
///////////////////////////////////////////////////////////////////////////////////
	public int jakiMiesiac() {
		int jM = d.getMonth();
		switch (jM) {
		case 0:	iloscDniWMiesiacu = 31;	break;
		case 1:	iloscDniWMiesiacu = 28;	break;
		case 2:	iloscDniWMiesiacu = 31; break;
		case 3:	iloscDniWMiesiacu = 30; break;
		case 4:	iloscDniWMiesiacu = 31;	break;
		case 5:	iloscDniWMiesiacu = 30;	break;
		case 6: iloscDniWMiesiacu = 31;	break;
		case 7:	iloscDniWMiesiacu = 31;	break;
		case 8:	iloscDniWMiesiacu = 30;	break;
		case 9:	iloscDniWMiesiacu = 31;	break;
		case 10: iloscDniWMiesiacu = 30; break;
		case 11: iloscDniWMiesiacu = 31; break;
		default:
			System.out.println("Brak wybranych ilo�ci dni w miesi�cu");
		}
		return iloscDniWMiesiacu;
	}
///////////////////////////////////////////////////////////////////////////////////
	public void ktoryDzienTygodniaToPierwszy(){
		
	}
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
}
