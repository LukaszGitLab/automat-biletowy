package Automat_biletowy_1;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class AdministratorBiletow {

	JFrame ramkaAdmin;
	
	public AdministratorBiletow(){
		tworzGUI();
	}

	private void tworzGUI() {

		ramkaAdmin = new JFrame("Administrator bilet�w");
		ramkaAdmin.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		
		JButton buttonDodajBilet = new JButton("Tworz nowy bilet");
		buttonDodajBilet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaAdmin.dispose();
				new TworzNowyBilet();
			}
		});
		panelA.add(buttonDodajBilet);
		
		JButton buttonEdytujBilet = new JButton("Edytuj bilety");
		buttonEdytujBilet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaAdmin.dispose();
				new EdycjaBiletow();
			}
		});
		panelA.add(buttonEdytujBilet);
		
		JButton buttonUsunBilet = new JButton("Usu� bilety");
		buttonUsunBilet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaAdmin.dispose();
				new UsunBilet();			
			}
		});
		panelA.add(buttonUsunBilet);
		
		JButton buttonUstalenieCenyUlgowej = new JButton("Ustalenie ceny ulgowej");
		buttonUstalenieCenyUlgowej.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaAdmin.dispose();
				new UstalenieCenyUlgowej();		
			}
		});
		panelA.add(buttonUstalenieCenyUlgowej);
		
		JButton wylaczAutomat = new JButton("Wy��cz automat");
		wylaczAutomat.setBackground(Color.BLACK);
		wylaczAutomat.setForeground(Color.WHITE);
		wylaczAutomat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0){
				System.exit(0);
			}
		});	
		panelA.add(wylaczAutomat);
		
		JButton buttonWyjscie = new JButton("Wyloguj");
		buttonWyjscie.setBackground(Color.BLACK);
		buttonWyjscie.setForeground(Color.WHITE);
		buttonWyjscie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaAdmin.dispose();
				new Automat();			
			}
		});
		panelA.add(buttonWyjscie);
		
		ramkaAdmin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramkaAdmin.add(panelA);
		ramkaAdmin.setSize(400, 600);
//		ramkaAdmin.pack();
		ramkaAdmin.setLocation(800, 500);
		ramkaAdmin.setLocationRelativeTo(null);
		ramkaAdmin.setVisible(true);
	}
}
