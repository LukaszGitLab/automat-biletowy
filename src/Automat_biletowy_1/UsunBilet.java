package Automat_biletowy_1;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;
import java.util.TreeSet;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class UsunBilet  {
	JFrame ramkaPrezentacja;
	JCheckBox[] polaWyboru = new JCheckBox[Automat.listaBiletow.size()];
	JButton[] listaButton = new JButton[Automat.listaBiletow.size()];
//	int i = 0;
	TreeSet<Integer> tabTreeSet = new TreeSet<Integer>();
	
	public UsunBilet(){
		tworzGUI();
	}

	private void tworzGUI() {
		ramkaPrezentacja = new JFrame("Edycja listy bilet�w");
		ramkaPrezentacja.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
//		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		GridLayout experimentLayout = new GridLayout(0,2,4,4);
		panelA.setLayout(experimentLayout);
//		JPasswordField passwd= new JPasswordField("Haslo");	panelA.add(passwd);
		
		for(int i = 0; i < Automat.listaBiletow.size(); i++){			
			listaButton[i] = new JButton(Automat.listaBiletow.get(i).getNazwaBiletu());
			listaButton[i].setName("" + i);
			listaButton[i].addActionListener(new ZaznaczDoUsuniecia());
			listaButton[i].setActionCommand("" + i);
			panelA.add(listaButton[i]);
			polaWyboru[i] = new JCheckBox();
//			polaWyboru[i].setSelected(false);
			panelA.add(polaWyboru[i]);
			polaWyboru[i].addItemListener(new SprawdzJCheckBox());
		}

		JButton buttonUsunWybraneBilety = new JButton("Usu� wybrane bilety");
		buttonUsunWybraneBilety.setBackground(Color.BLACK);
		buttonUsunWybraneBilety.setForeground(Color.WHITE);
		buttonUsunWybraneBilety.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				ramkaPrezentacja.dispose();
				System.out.println("TreeSet = " + tabTreeSet);
				Iterator<Integer> it = tabTreeSet.descendingIterator();
				while(it.hasNext()) {
//					System.out.print(", it.next() = " + it.next() + ", " + it.next() + ";");
					int k = (int) it.next();
					if(polaWyboru[k].isSelected()){
//						Automat.listaBiletow.remove((int) it.next());
						Automat.listaBiletow.remove(k);
					}
					else {System.out.println("Ten bilet odznaczono " + k);}
//					
//					Automat.listaBiletow.remove(0);
				}
//				for(int i = 0; i < polaWyboru.length; i++) {
//					
//					if(tabTreeSet.descendingSet().contains(i)) {
//						
//						Automat.listaBiletow.remove(i);
//						System.out.println("Usuni�to bilet " + i + ", ");
//					}
//				}
				
//				new UsunBilet();
//				new Automat();
				new AdministratorBiletow();
			}
		});
		panelA.add(buttonUsunWybraneBilety);
		
		JButton buttonWyjscie = new JButton("Anuluj");
		buttonWyjscie.setBackground(Color.BLACK);
		buttonWyjscie.setForeground(Color.WHITE);
		buttonWyjscie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaPrezentacja.dispose();
				new AdministratorBiletow();			
			}
		});
		panelA.add(buttonWyjscie);
		
		ramkaPrezentacja.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramkaPrezentacja.add(panelA);
//		ramkaPrezentacja.setSize(400, 600);
		ramkaPrezentacja.pack();
		ramkaPrezentacja.setLocation(400, 100);
		ramkaPrezentacja.setLocationRelativeTo(null);
		ramkaPrezentacja.setVisible(true);	
	}
/////////////////////////////////////////////////////////////////////////////////////	
	class ZaznaczDoUsuniecia implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent zd) {
//			int k = Integer.parseInt(zd.getActionCommand());

			System.out.println("= " + zd.getActionCommand());
			System.out.println("polaWyboru.length = " + polaWyboru.length);
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class SprawdzJCheckBox implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			for(int i = 0; i < polaWyboru.length; i++) {
				if(polaWyboru[i].isSelected()) {
					tabTreeSet.add(Integer.parseInt(listaButton[i].getActionCommand()));
					System.out.println("Wybrano do usuni�cia bilet nr " + i);
				}
			}		
//			Automat.listaBiletow.remove(0);

		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
}
