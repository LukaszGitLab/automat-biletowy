package Automat_biletowy_1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class UstalenieCenyUlgowej {

	static float dzielnikCenyUlgowej = 0.50f;
	JFrame ramkaUstalenieCenyUlgowej;
	JLabel textUlga;
	
	public UstalenieCenyUlgowej() {
		tworzGUI();
	}
	
	public void tworzGUI() {
		ramkaUstalenieCenyUlgowej = new JFrame("Administrator bilet�w");
		ramkaUstalenieCenyUlgowej.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
//		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		panelA.setLayout( new GridLayout(0,2));
		
//		JLabel label1 = new JLabel("Lukasz");
//		Border border = BorderFactory.createLineBorder(Color.BLACK);
//		label1.setBorder(border);
//		label1.setPreferredSize(new Dimension(150, 100));
//		label1.setHorizontalAlignment(JLabel.CENTER);
//	    label1.setVerticalAlignment(JLabel.CENTER);
//		panelA.add(label1);
//	
//		JLabel label2 = new JLabel("Lukasz");
//		Border border2 = BorderFactory.createLineBorder(Color.RED);
//		label2.setBorder(border2);
//		panelA.add(label2);
//		JLabel label3 = new JLabel("Lukasz Kepka");
//		Border border3 = BorderFactory.createLineBorder(Color.RED);
//		label3.setBorder(border3);
//		panelA.add(label3);
//		JLabel label4 = new JLabel("Lukasz");
//		Border border4 = BorderFactory.createLineBorder(Color.BLUE);
//		label4.setBorder(border4);
//		panelA.add(label4);
		
		Font font = new Font("serif", Font.BOLD, 16);
		JLabel wartoscUlgi = new JLabel("Warto�� ulgi [%] ");panelA.add(wartoscUlgi);wartoscUlgi.setFont(font);	
		textUlga = new JLabel("" + String.format("%.0f", dzielnikCenyUlgowej * 100));panelA.add(textUlga);textUlga.setFont(font);
		textUlga.setHorizontalAlignment(JLabel.CENTER);
//		JLabel labelProcent = new JLabel(" % ");panelA.add(labelProcent);
//		JLabel labelPusty = new JLabel("");panelA.add(labelPusty);
		JButton buttonPlus = new JButton("+");panelA.add(buttonPlus);buttonPlus.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent a) {
				if(dzielnikCenyUlgowej < 0.99f) dzielnikCenyUlgowej = dzielnikCenyUlgowej + 0.01f;
				textUlga.setText("" + String.format("%.0f", dzielnikCenyUlgowej * 100));
				System.out.println("Plus Ulga = " + String.format("%.2f", dzielnikCenyUlgowej));
			}
		});
		
		JButton buttonMinus = new JButton("-");panelA.add(buttonMinus);buttonMinus.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent a) {
				if(dzielnikCenyUlgowej > 0.01f) dzielnikCenyUlgowej = dzielnikCenyUlgowej - 0.01f;
				textUlga.setText("" + String.format("%.0f", dzielnikCenyUlgowej * 100));
				System.out.println("Minus Ulga = " + String.format("%.2f", dzielnikCenyUlgowej));
			}
		});
		
		JButton buttonWyjscie = new JButton("Zapisz zmiany");
		buttonWyjscie.setBackground(Color.BLACK);
		buttonWyjscie.setForeground(Color.WHITE);
		buttonWyjscie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				ramkaUstalenieCenyUlgowej.dispose();
				new AdministratorBiletow();		
			}
		});
		panelA.add(buttonWyjscie);
		
//		JSpinner spin = new JSpinner();
//		spin.setModel(new SpinnerNumberModel(50, 0, 100, 1));	
//		panelA.add(spin);
//		ChangeListener listener = new ChangeListener() {
//			@Override
//			public void stateChanged(ChangeEvent e) {
////				JOptionPane.showMessageDialog(null, "wynik " + e.getSource());
//				JOptionPane.showMessageDialog(null, "wynik " + spin.getValue());
//			}			
//		};
//		spin.addChangeListener(listener);
		
		ramkaUstalenieCenyUlgowej.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramkaUstalenieCenyUlgowej.add(panelA);
		ramkaUstalenieCenyUlgowej.setSize(400, 600);
	//	ramkaUstalenieCenyUlgowej.pack();
		ramkaUstalenieCenyUlgowej.setLocation(1200, 0);
		ramkaUstalenieCenyUlgowej.setLocationRelativeTo(null);
		ramkaUstalenieCenyUlgowej.setVisible(true);
	}
	
	
}
