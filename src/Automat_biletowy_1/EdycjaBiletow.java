package Automat_biletowy_1;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import org.w3c.dom.events.EventTarget;
import org.w3c.dom.views.AbstractView;

public class EdycjaBiletow {

	JFrame ramkaPrezentacja;
	JCheckBox[] polaWyboru = new JCheckBox[Automat.listaBiletow.size()];
	JButton[] listaButton = new JButton[Automat.listaBiletow.size()];
	JLabel[] listaNazw = new JLabel[Automat.listaBiletow.size()];
	JLabel[] listaCen = new JLabel[Automat.listaBiletow.size()];
	JTextField[] listaTextFieldNazwaBiletu = new JTextField[Automat.listaBiletow.size()];
//	JTextField[] listaTextFieldCenaBiletu = new JTextField[Automat.listaBiletow.size()];
	JTextField[] listaTextFieldCenaBiletu = new JTextField[Automat.listaBiletow.size()];
	ArrayList<Integer> tabPINKcena = new ArrayList<Integer>();
	ArrayList<Integer> tabPINKnazwa = new ArrayList<Integer>();
//	boolean flagaPrzecinkaIKropki = false;
//	boolean flagaPierwszegiZnaku = false;
	boolean[] czyPoleMaPierwszyZnak = new boolean[Automat.listaBiletow.size()];
	boolean[] czyPoleMaPrzecinek = new boolean[Automat.listaBiletow.size()];
	JButton buttonZapiszZmiany;
	JButton buttonPowrot;
	JButton buttonAnuluj;
	
	public EdycjaBiletow(){
		tworzGUI();
	}
	
	private void tworzGUI() {
		ramkaPrezentacja = new JFrame("Edycja listy bilet�w");
		ramkaPrezentacja.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
//		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		GridLayout experimentLayout = new GridLayout(0,4,4,4);
		panelA.setLayout(experimentLayout);
//		JPasswordField passwd= new JPasswordField("Haslo");	panelA.add(passwd);
		Font font = new Font("serif", Font.BOLD, 24);//labelPowitanie.setFont(font);
		Font font16 = new Font("serif", Font.BOLD, 16);
		JLabel listaBiletow = new JLabel("Lista bilet�w"); listaBiletow.setFont(font);panelA.add(listaBiletow);
		JLabel aktualnaCenaBiletow = new JLabel("Aktualna Cena Bilet�w"); aktualnaCenaBiletow.setFont(font);panelA.add(aktualnaCenaBiletow);
		JLabel edytujNazweBiletu = new JLabel("Edytuj nazw� biletu"); edytujNazweBiletu.setFont(font);panelA.add(edytujNazweBiletu);
		JLabel edytujCeneBiletu = new JLabel("Edytuj cen� biletu"); edytujCeneBiletu.setFont(font);panelA.add(edytujCeneBiletu);
			
		for(int i = 0; i < Automat.listaBiletow.size(); i++){					

			listaNazw[i] = new JLabel(""+ Automat.listaBiletow.get(i).getNazwaBiletu());listaNazw[i].setFont(font16);
			panelA.add(listaNazw[i]);
			
			listaCen[i] = new JLabel(""+ String.format ("%.2f",Automat.listaBiletow.get(i).getCenaBiletu()) + " z�");listaCen[i].setFont(font16);
			panelA.add(listaCen[i]);
			
			listaTextFieldNazwaBiletu[i] = new JTextField(""+Automat.listaBiletow.get(i).getNazwaBiletu());
			panelA.add(listaTextFieldNazwaBiletu[i]);
						
//			listaTextFieldCenaBiletu[i] = new JTextField(""+ String.format ("%.2f",Automat.listaBiletow.get(i).getCenaBiletu()));
			listaTextFieldCenaBiletu[i] = new JTextField(""+ String.format ("%.2f",Automat.listaBiletow.get(i).getCenaBiletu()));
			listaTextFieldCenaBiletu[i].setName("" + i);
			listaTextFieldCenaBiletu[i].addKeyListener(new CzyLiczba());

			listaTextFieldCenaBiletu[i].addMouseListener(new MouseAdapter() {
				 @Override
		            public void mouseClicked(MouseEvent e){
					 	System.out.println("Mysz");
		            }
			});
			listaTextFieldCenaBiletu[i].addMouseListener(new Wyzeruj());
		
			if(tabPINKnazwa.contains(i)) {
				listaTextFieldNazwaBiletu[i].setBackground(Color.PINK);
				listaTextFieldNazwaBiletu[i].setText("");
				System.out.println("tabPINKnazwa.indexOf(i) = " + tabPINKnazwa.indexOf(i));
			}
			
			if(tabPINKcena.contains(i)) {
				listaTextFieldCenaBiletu[i].setBackground(Color.PINK);
				listaTextFieldCenaBiletu[i].setText("");
				System.out.println("tabPINKcena.indexOf(i) = " + tabPINKcena.indexOf(i));
			}
			
			panelA.add(listaTextFieldCenaBiletu[i]);
		}
		
//		tabPINKnazwa.clear(); /////////////////////////////////////////
//		tabPINKcena.clear();		////////////////////////////////////////////////////
		
		buttonZapiszZmiany = new JButton("Zapisz zmiany");
		buttonZapiszZmiany.setBackground(Color.BLACK);
		buttonZapiszZmiany.setForeground(Color.WHITE);
//		buttonZapiszZmiany.addActionListener(new ZapiszZmiany());
		buttonZapiszZmiany.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaPrezentacja.dispose();				
				for(int i= 0; i < Automat.listaBiletow.size(); i++){	
					// 1
					if(!listaTextFieldNazwaBiletu[i].getText().equals("")) {
						Automat.listaBiletow.get(i).setNazwaBiletu(listaTextFieldNazwaBiletu[i].getText());

					}
					else{
						tabPINKnazwa.add(i);
						System.out.println("Brak nazwy biletu");
						buttonPowrot.setVisible(false);

					}
					// 2
					try{
						if(NumberFormat.getInstance().parse(listaTextFieldCenaBiletu[i].getText().toString().replace(".", ",")).floatValue() != 0){
							Automat.listaBiletow.get(i).setCenaBiletu(NumberFormat.getInstance().parse(listaTextFieldCenaBiletu[i].getText().toString().replace(".", ",")).floatValue());
						}
						else{
							listaTextFieldCenaBiletu[i].setBackground(Color.PINK);
							System.out.println("Cena nr " + i + " to zero");
							tabPINKcena.add(i);
							buttonPowrot.setVisible(false);
						}
//						Automat.listaBiletow.get(i).setCenaBiletu(NumberFormat.getInstance().parse(listaTextFieldCenaBiletu[i].getText().toString().replace(".", ",")).floatValue()); // dzia�a
					} catch(Exception e){
//						e.printStackTrace();
						listaTextFieldCenaBiletu[i].setBackground(Color.PINK);
						System.out.println("Cena nr " + i + " to nie liczba");
						tabPINKcena.add(i);
						buttonPowrot.setVisible(false);
					}
				}
				
				//
				for(int i = 0; i < Automat.listaBiletow.size(); i++) {
					czyPoleMaPierwszyZnak[i] = false;
					czyPoleMaPrzecinek[i] = false;
				}
				//
				System.out.println("\ntabPINKnazwa.isEmpty() && tabPINKcena.isEmpty(), " + tabPINKnazwa.isEmpty() + ", " + tabPINKcena.isEmpty() + " = " + (tabPINKnazwa.isEmpty() && tabPINKcena.isEmpty()));
				if((tabPINKcena.isEmpty() && tabPINKnazwa.isEmpty())){
//					buttonPowrot.setVisible(true);
					System.out.println("Czy pokaza� button? " + (tabPINKcena.isEmpty() && tabPINKnazwa.isEmpty()));
				}
				//
				
				System.out.println("tabPINKnazwa " + tabPINKnazwa + ", tabPINKcena " + tabPINKcena);
				tworzGUI();
//				new EdycjaBiletow();
			}
		});
			
		panelA.add(buttonZapiszZmiany);
		
		buttonPowrot = new JButton("Powr�t");
		buttonPowrot.setBackground(Color.BLACK);
		buttonPowrot.setForeground(Color.WHITE);
		buttonPowrot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaPrezentacja.dispose();
				for(int i = 0; i < Automat.listaBiletow.size(); i++) {
					czyPoleMaPierwszyZnak[i] = false;
					czyPoleMaPrzecinek[i] = false;
				}
				new AdministratorBiletow();		
								
			}
		});

		if((tabPINKcena.isEmpty() && tabPINKnazwa.isEmpty())){
			buttonPowrot.setVisible(true);
//			System.out.println("Czy pokaza� button? " + (tabPINKcena.isEmpty() && tabPINKnazwa.isEmpty()));
		}
		else {
			buttonPowrot.setVisible(false);			
		}
		panelA.add(buttonPowrot);
		
	
		buttonAnuluj = new JButton("Anuluj");
		buttonAnuluj.setBackground(Color.BLACK);
		buttonAnuluj.setForeground(Color.WHITE);
		buttonAnuluj.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramkaPrezentacja.dispose();
				for(int i = 0; i < Automat.listaBiletow.size(); i++) {
					czyPoleMaPierwszyZnak[i] = false;
					czyPoleMaPrzecinek[i] = false;
				}
//				flagaPrzecinkaIKropki = false;
//				flagaPierwszegiZnaku = false;
				new AdministratorBiletow();			
			}
		});
		
		if((tabPINKcena.isEmpty() && tabPINKnazwa.isEmpty())){
			buttonAnuluj.setVisible(true);
//			System.out.println("Czy pokaza� button? " + (tabPINKcena.isEmpty() && tabPINKnazwa.isEmpty()));
		}
		else {
			buttonAnuluj.setVisible(false);			
		}
		panelA.add(buttonAnuluj);
			
		tabPINKnazwa.clear();
		tabPINKcena.clear();
		
		ramkaPrezentacja.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramkaPrezentacja.add(panelA);
		ramkaPrezentacja.setSize(400, 600);
		ramkaPrezentacja.pack();
		ramkaPrezentacja.setLocation(300, 0);
		ramkaPrezentacja.setLocationRelativeTo(null);
		ramkaPrezentacja.setVisible(true);	
	}
/////////////////////////////////////////////////////////////////////////////////////
	class CzyLiczba implements KeyListener{

		@Override
		public void keyPressed(KeyEvent e) {		
		}
		@Override
		public void keyReleased(KeyEvent e) {
		}
		@Override
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar(); 
//			System.out.println("e.getSource() = " + e.getSource());
//			System.out.println("e.getSource() = " + e.getComponent().getLocation());
			JTextField tF = (JTextField) e.getSource();
//			System.out.println("tF.getText() = " + tF.getName());
			
			if(!czyPoleMaPierwszyZnak[Integer.parseInt(tF.getName())]){
				if(!(Character.isDigit(c) ||  c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE )) {
					Toolkit.getDefaultToolkit().beep();    						
					e.consume();					
				}
				else{
//					flagaPierwszegiZnaku = true;
					czyPoleMaPierwszyZnak[Integer.parseInt(tF.getName())] = true;
				}
//				System.out.println("if nr 1");
			}
			else if(czyPoleMaPierwszyZnak[Integer.parseInt(tF.getName())]){
//				if(!flagaPrzecinkaIKropki){
				if(!czyPoleMaPrzecinek[Integer.parseInt(tF.getName())]){					
					if(!(Character.isDigit(c) ||  c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE || c == KeyEvent.VK_COMMA || c == KeyEvent.VK_PERIOD)) {
						Toolkit.getDefaultToolkit().beep();    						
						e.consume();						
//						System.out.println("if nr 2");
					}
					else{
						if(c == KeyEvent.VK_COMMA || c == KeyEvent.VK_PERIOD){
//							flagaPrzecinkaIKropki = true;
							System.out.println("To by� przecinek lub kropka");
							czyPoleMaPrzecinek[Integer.parseInt(tF.getName())] = true;
						}
					}
				}
				else{
					if(!(Character.isDigit(c) ||  c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE )) {
						Toolkit.getDefaultToolkit().beep();    						
						e.consume();
						
					}
//					System.out.println("if nr 3");
				}
			}
//			System.out.println("flagaPierwszegiZnaku = "+ flagaPierwszegiZnaku + ", flagaPrzecinkaIKropki = " + flagaPrzecinkaIKropki +
//					", wynik = " + (flagaPierwszegiZnaku && flagaPrzecinkaIKropki));
		}
	}
/////////////////////////////////////////////////////////////////////////////////////
	class Wyzeruj implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {

			
		}
	
		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void mousePressed(MouseEvent e) {
//			listaTextFieldCenaBiletu[2].setText(""); // dzia�a
			JTextField tF = (JTextField) e.getSource();
//			System.out.println("Mysz2");			
			listaTextFieldCenaBiletu[Integer.parseInt(tF.getName())].setText("");
			
		}
	
		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	}
/////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////
//	private void jTextFieldKeyTyped
/////////////////////////////////////////////////////////////////////////////////////
}
