package Automat_biletowy_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.GregorianCalendar;
import java.util.Timer;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ProgressBarUI;


import javax.swing.SwingUtilities; //

public class Zaplac implements ActionListener {

	JFrame ramkaZaplac;
	JLabel labelWartoscDoZaplaty;
	String[] listaUlgString = {"Wybierz rodzaj dodatkowej ulgi" , "Senior 60+ ( 10% )", "Senior 70+ ( 20% )"};
	JComboBox optionsCombo;
	static int optionsComboWybrane = 0;
	String[] listaRodzajowBiletow = {"Bilet papierowy", "Bilet elektroniczny"};
	JComboBox rodzajBiletu;
	static int rodzajBiletuWybrane = 0;
	JCheckBox polePrzylozKarte;
	JProgressBar prog;
	JProgressBar progressBar;
	static final int interval = 1000;
	int licznik = 0;
	
	JButton przyciskKontynuujZakupy;
	JButton przyciskRezygnacja;	

	float cenaRazemPodsumowanieGlowne = PrezentacjaBiletow.cenaRazemPodsumowanieStatic;
//	float cenaRazemPodsumowanieZUlga = PrezentacjaBiletow.cenaRazemPodsumowanieStatic;
//	ImageIcon icon;
//	Image obrazek;
	
//	BufferedImage img;
//	String img1 = "/imgs/bilet3.jpg";
//	String img2 = "/imgs/urbancard.jpg";
//	String img3 = ".\\imgs\\bilet3.jpg";
	
	JFrame ramkaBilet;
	
	public Zaplac() {
		tworzGUI();
	}
	
	public void tworzGUI() {
		ramkaZaplac = new JFrame("Zap�ac");
		ramkaZaplac.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
//		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		panelA.setLayout( new GridLayout(0,2));
		
		Border border2 = BorderFactory.createLineBorder(Color.BLUE);
		Font font18 = new Font("serif", Font.BOLD, 18);
		JLabel labelTekstDoZaplaty = new JLabel("Kwota do zap�aty z�");panelA.add(labelTekstDoZaplaty);labelTekstDoZaplaty.setFont(font18);
		labelWartoscDoZaplaty = new JLabel("" + String.format("%.2f", cenaRazemPodsumowanieGlowne));panelA.add(labelWartoscDoZaplaty);labelWartoscDoZaplaty.setFont(font18);
		labelWartoscDoZaplaty.setBorder(border2);
		
		przyciskKontynuujZakupy = new JButton("Kontynuuj zakupy");	
		przyciskKontynuujZakupy.setForeground(Color.BLACK);
		przyciskKontynuujZakupy.setBackground(Color.WHITE);
		przyciskKontynuujZakupy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				arrayPodsumowanie.add(new Bufor());
				ramkaZaplac.dispose();
				optionsComboWybrane = optionsCombo.getSelectedIndex();
				rodzajBiletuWybrane = rodzajBiletu.getSelectedIndex();
				new PrezentacjaBiletow();
			}
		});
		panelA.add(przyciskKontynuujZakupy);
		
		przyciskRezygnacja = new JButton("Rezygnacja z zakup�w");	
		przyciskRezygnacja.setForeground(Color.WHITE);
		przyciskRezygnacja.setBackground(Color.BLACK);
		przyciskRezygnacja.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ramkaZaplac.dispose();
				PrezentacjaBiletow.arrayPodsumowanie.clear();
				optionsComboWybrane = 0;
				rodzajBiletuWybrane = 0;

				new PrezentacjaBiletow();
			}
		});
		panelA.add(przyciskRezygnacja);
		
		// 1
		optionsCombo = new JComboBox(listaUlgString);
		optionsCombo.setSelectedIndex(optionsComboWybrane); // pocz�tkowa wybrana pozycja
//		optionsCombo.set
		optionsCombo.addActionListener(new WybierzDodatkowaZnizke());
		panelA.add(optionsCombo);
		// 2
		rodzajBiletu = new JComboBox(listaRodzajowBiletow);
		rodzajBiletu.setSelectedIndex(rodzajBiletuWybrane); // pocz�tkowa wybrana pozycja
//		optionsCombo.set
		rodzajBiletu.addActionListener(new WybierzRodzajBiletu());
		panelA.add(rodzajBiletu);
		
		polePrzylozKarte = new JCheckBox("Przy�� kart�");panelA.add(polePrzylozKarte);polePrzylozKarte.addItemListener(new SprawdzJCheckBox());		

		prog = new JProgressBar(0,100);
		prog.setVisible(false);
		prog.setString("Trwa uwierzytelnianie");
		prog.setValue(licznik);
		prog.setStringPainted(true);
		panelA.add(prog);
		
		
//		progressBar = new JProgressBar(0, 100);
//		progressBar.setValue(10);
//		progressBar.setStringPainted(true);
//		panelA.add(progressBar);
		
		ramkaZaplac.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramkaZaplac.add(panelA);
		ramkaZaplac.setSize(450, 500);
	//	ramkaZaplac.pack();
		ramkaZaplac.setLocation(800, 500);
		ramkaZaplac.setLocationRelativeTo(null);
		ramkaZaplac.setVisible(true);
	}
/////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void actionPerformed(ActionEvent arg0) {
		ramkaBilet.dispose();
	}
/////////////////////////////////////////////////////////////////////////////////////
	class WybierzDodatkowaZnizke implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("Selected: " + optionsCombo.getSelectedItem());
	        System.out.println(", Position: :" + optionsCombo.getSelectedIndex());
	       
	        for(int i = 0; i < listaUlgString.length; i++) {
	        	switch(optionsCombo.getSelectedIndex()) {
	        		case 0: 
	        			cenaRazemPodsumowanieGlowne = PrezentacjaBiletow.cenaRazemPodsumowanieStatic;
	        			labelWartoscDoZaplaty.setText("" + String.format("%.2f",cenaRazemPodsumowanieGlowne));
	        			break;
	        		case 1:
	        			cenaRazemPodsumowanieGlowne = (float) PrezentacjaBiletow.cenaRazemPodsumowanieStatic * 0.90f;
	        			labelWartoscDoZaplaty.setText("" + String.format("%.2f",cenaRazemPodsumowanieGlowne));
	        			break;
	        		case 2:
	        			cenaRazemPodsumowanieGlowne = (float) PrezentacjaBiletow.cenaRazemPodsumowanieStatic * 0.80f;
	        			labelWartoscDoZaplaty.setText("" + String.format("%.2f",cenaRazemPodsumowanieGlowne));
	        			break;
	        		default:
	        			cenaRazemPodsumowanieGlowne = PrezentacjaBiletow.cenaRazemPodsumowanieStatic;
	        			labelWartoscDoZaplaty.setText("" + String.format("%.2f",cenaRazemPodsumowanieGlowne));
	        			break;
	        	}
	        }
			
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class SprawdzJCheckBox implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			
			if(polePrzylozKarte.isSelected()){
				polePrzylozKarte.setEnabled(false);
				System.out.println("Zap�acono");
				prog.setVisible(true);
				przyciskKontynuujZakupy.setVisible(false);
				przyciskRezygnacja.setVisible(false);		
				optionsCombo.setVisible(false);
				rodzajBiletu.setVisible(false);
				
				Thread watekOdliczanie = new Thread(new Odliczanie());
				watekOdliczanie.start();

//				// z internetu
//				Thread t = new Thread(){
//			        public void run(){
//			            for(int i = 0 ; i < 100 ; i++){
//			                final int percent = i;
//			                SwingUtilities.invokeLater(new Runnable() {
//			                    public void run() {
//			                        prog.setValue(percent);
//			                    }
//			                  });
//
//			                try {
//			                    Thread.sleep(50);
//			                } catch (InterruptedException e) {}
//			            }
//			        }
//			    };
//			    t.start();
					
			}
			else{
				prog.setVisible(false);
				przyciskKontynuujZakupy.setVisible(true);
				przyciskRezygnacja.setVisible(true);
				optionsCombo.setVisible(true);
				rodzajBiletu.setVisible(true);
			}
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class WybierzRodzajBiletu implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("Selected Biletu: " + rodzajBiletu.getSelectedItem());
	        System.out.println(", Position Biletu: :" + rodzajBiletu.getSelectedIndex());
			
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class Odliczanie implements Runnable{
		@Override
		public void run() {
			
			Runnable zadanieWatku = new Runnable() {
				@Override
				public void run() {
					licznik = 0;
					prog.setValue(licznik);
					while(licznik < 100) {
						
						System.out.println("licznik = " + licznik);
						try{
						Thread.sleep(40);
						licznik++;
						prog.setValue(licznik);						
						} catch(Exception ex){
							ex.printStackTrace();
						}
					}
					if(licznik == 100) {
						
						ramkaZaplac.dispose();
						PrezentacjaBiletow.arrayPodsumowanie.clear();
						new PrezentacjaBiletow();
						wyswietlBilet(); 
					}
				}
			};
			Thread mojWatek = new Thread(zadanieWatku);
			mojWatek.start();
					
		}
	}
/////////////////////////////////////////////////////////////////////////////////////
	public void wyswietlBilet() {
		
		// z internetu
//		JFrame frame = new JFrame();
//		ImageIcon icon = new ImageIcon("bilet3.jpg");
//		JLabel label = new JLabel(icon);
//		frame.add(label);
//		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
//		frame.pack();
//		frame.setResizable(false);
//		frame.setVisible(true);
		

//		JFrame 
		ramkaBilet = new JFrame("Bilet");
		ramkaBilet.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imgs/bilet3.jpg")));
		JPanel panelA = new JPanel();
		ramkaBilet.add(panelA);
			
//		JToolBar tb = new JToolBar();
		ImageIcon icon = new ImageIcon(Zaplac.class.getResource("/imgs/bilet3.jpg"));
//		tb.add(new JButton(icon2));
//		ramkaBilet.add(tb,BorderLayout.NORTH);
		
		if(rodzajBiletu.getSelectedIndex() == 0){
//			icon = new ImageIcon(".\\src\\Automat_biletowy_1\\bilet3.jpg"); // dla Eclipse
			icon = new ImageIcon(Zaplac.class.getResource("/imgs/bilet3.jpg"));
			ramkaBilet.setTitle("Bilet papierowy");
		}
//			
		if(rodzajBiletu.getSelectedIndex() == 1){
//			icon = new ImageIcon(".\\src\\Automat_biletowy_1\\urbancard.jpg"); // dla Eclipse
			icon = new ImageIcon(Zaplac.class.getResource("/imgs/urbancard.jpg"));
			ramkaBilet.setTitle("Bilet elektroniczny: Urbancard");
		}
		
//		JLabel labelObrazek = new JLabel(icon);
//		ramkaBilet.add(labelObrazek);
		JButton buttonIcon = new JButton(icon);
		buttonIcon.addActionListener(this);
		ramkaBilet.add(buttonIcon);

		ramkaBilet.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//		ramkaBilet.setSize(300,300);
		ramkaBilet.pack();
//		ramkaBilet.setResizable(false);
//		ramkaBilet.setLocation(500, 500);
		ramkaBilet.setLocationRelativeTo(null); // ustawienie na �rodku ekranu
		ramkaBilet.setVisible(true);
			
			
		
		optionsComboWybrane = 0;
		rodzajBiletuWybrane = 0;
		
	}
/////////////////////////////////////////////////////////////////////////////////////
//	class MojPanelRysunkowy extends JPanel {
//		public void paintComponent(Graphics g) {
////			Image obrazek = new ImageIcon("D:\\Profiles\\L.Kepka\\eclipse-workspace\\Moje04a_Automat_biletowy2\\bilet3.jpg").getImage();
//			Image obrazek = new ImageIcon("bilet3.jpg").getImage();
//			try {
//				obrazek = ImageIO.read(new File(".\\src\\bilet3.jpg"));
//
////				img = ImageIO.read(Zaplac.class.getResource(img1));
////				Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imgs/bilet3.jpg"))
//			} catch(IOException e) {
//				e.printStackTrace();
//			}
//			g.drawImage(obrazek,0,0,this);
////			g.drawImage(obrazek, 0, 0, getWidth(), getHeight(), this);	
//		}
//	}
/////////////////////////////////////////////////////////////////////////////////////
}
