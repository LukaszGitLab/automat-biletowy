package Automat_biletowy_1;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Automat {
	
	public Automat() {
		tworzGUI();	
	}
	
	static ArrayList<Bilet> listaBiletow = new ArrayList<Bilet>();
	
	JFrame ramka;
	
	public static void main(String[] args) {
//		Automat automat = new Automat();
//		automat.tworzBilety();
//		automat.tworzGUI();
		tworzBilety(); 
		new Automat();
		
	}
	private static void tworzBilety() {
		Bilet b1 = new Bilet("Jednorazowy", 3); listaBiletow.add(b1);
		Bilet b2 = new Bilet("Czasowy 30 min", 3); listaBiletow.add(b2);
		Bilet b3 = new Bilet("Czasowy 60 min", 4); listaBiletow.add(b3);
		Bilet b4 = new Bilet("Czasowy 90 min", 6); listaBiletow.add(b4);
		Bilet b5 = new Bilet("Dzienny 24h", 11); listaBiletow.add(b5);
		Bilet b6 = new Bilet("Dzienny 48h", 20); listaBiletow.add(b6);
		Bilet b7 = new Bilet("Dzienny 72h", 26); listaBiletow.add(b7);
		Bilet b8 = new Bilet("30-dniowy dwie dowolne linie", 60); listaBiletow.add(b8);
		Bilet b9 = new Bilet("30-dniowy wszystkie linie", 98); listaBiletow.add(b9);
		Bilet b10 = new Bilet("60-dniowy wszystkie linie", 180); listaBiletow.add(b10);
		Bilet b11 = new Bilet("90-dniowy wszystkie linie", 240); listaBiletow.add(b11);
		Bilet b12 = new Bilet("Na okaziciela 30-dniowy", 170); listaBiletow.add(b12);
		Bilet b13 = new Bilet("Na okaziciela 60-dniowy", 310); listaBiletow.add(b13);
		Bilet b14 = new Bilet("Na okaziciela 90-dniowy", 430); listaBiletow.add(b14);
		Bilet b15 = new Bilet("Semestralny 4-miesi�czny wszystkie linie", 300); listaBiletow.add(b15);
		Bilet b16 = new Bilet("Semestralny 5-miesi�czny wszystkie linie", 370); listaBiletow.add(b16);
		listaBiletow.add(new Bilet("Nocny", 16.42f));
	}
	public void tworzGUI() {
		ramka = new JFrame("Automat biletowy, �ukasz K�pka 01.2018");
		ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramka.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		
		JButton kupBilet = new JButton("Kup bilet");panelA.add(kupBilet);
		kupBilet.addActionListener(new KupBilet());
		JButton zalogujJakoAdministrator = new JButton("Zaloguj jako administrator");panelA.add(zalogujJakoAdministrator);
		zalogujJakoAdministrator.addActionListener(new ZalogujJakoAdministrator());	
		
		JButton wylaczAutomat = new JButton("Wy��cz automat");panelA.add(wylaczAutomat);
		wylaczAutomat.setBackground(Color.BLACK);
		wylaczAutomat.setForeground(Color.WHITE);
		wylaczAutomat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0){
				System.exit(0);
			}
		});	
		
		ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramka.add(panelA);
		ramka.setSize(400, 500);
//		ramka.pack();
		ramka.setLocation(0, 0);
		ramka.setLocationRelativeTo(null);
		ramka.setVisible(true);	
	}
/////////////////////////////////////////////////////////////////////////
	class ZalogujJakoAdministrator implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			ramka.dispose();
			new WpiszHaslo();			
		}		
	}
/////////////////////////////////////////////////////////////////////////
	class KupBilet implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			ramka.dispose();
			new PrezentacjaBiletow();
		}
	}
/////////////////////////////////////////////////////////////////////////
}
