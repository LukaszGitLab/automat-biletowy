package Automat_biletowy_1;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TworzNowyBilet {
	JFrame ramka;
	JTextField nazwaBiletu;
	JTextField cenaBiletu;
	
	public TworzNowyBilet(){
		tworzGUI();
	} 
	public void tworzGUI() {
		
		ramka = new JFrame("Tworzenie nowego biletu");
//		ramka.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
//		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		
		JLabel labelNazwaBiletu = new JLabel("Nazwa biletu");
		panelA.add(labelNazwaBiletu);
		nazwaBiletu = new JTextField(25);
		panelA.add(nazwaBiletu);
		
//		JLabel info = new JLabel("CEN� PODAWA� Z PRZECINKIEM, NIE U�YWA� KROPKI");
//		info.setForeground(Color.RED);
//		panelA.add(info);
		
		JLabel labelCenaBiletu = new JLabel("Cena biletu");
		panelA.add(labelCenaBiletu);
		cenaBiletu = new JTextField(25);
		panelA.add(cenaBiletu);
		
		JButton zatwierdzButton = new JButton("Zatwierd�");
		zatwierdzButton.addActionListener(new ZatwierdzNowyBilet());
		panelA.add(zatwierdzButton);
		
		JButton buttonWyjscie = new JButton("Powr�t");
		buttonWyjscie.setBackground(Color.BLACK);
		buttonWyjscie.setForeground(Color.WHITE);
		buttonWyjscie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ramka.dispose();
				new AdministratorBiletow();			
			}
		});
		panelA.add(buttonWyjscie);
		
		ramka.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		ramka.add(panelA);
		ramka.setSize(400, 500);
//		ramka.pack();
		ramka.setLocation(400, 0);
		ramka.setLocationRelativeTo(null);
		ramka.setVisible(true);
	}
	
	class ZatwierdzNowyBilet implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			ramka.dispose();
			float cenaB = 0.00f;
			try{
				cenaB = NumberFormat.getInstance().parse(cenaBiletu.getText().toString().replace(".", ",")).floatValue();
			} catch(Exception ex2){
				ex2.printStackTrace();
			}
//			Bilet b = new Bilet(nazwaBiletu.getText(), (float) Float.parseFloat(cenaBiletu.getText()));
			Bilet b = new Bilet(nazwaBiletu.getText(), cenaB);
			Automat.listaBiletow.add(b);
//			new PrezentacjaBiletow();
//			new AdministratorBiletow();
			new TworzNowyBilet();
		}
		
	}

}
