package Automat_biletowy_1;

import javax.swing.JPanel;

public class Bilet extends JPanel {

	private String nazwaBiletu = "BRAK NAZWY BILETU";
	private float cenaBiletu = 0;
	
	public Bilet(String nazwaBiletu, float cenaBiletu) {
		this.nazwaBiletu = nazwaBiletu;
		this.cenaBiletu = cenaBiletu;
	}
	
	public String getNazwaBiletu() {
		return nazwaBiletu;
	}
	public void setNazwaBiletu(String nazwaBiletu) {
		this.nazwaBiletu = nazwaBiletu;
	}
	public float getCenaBiletu() {
		return cenaBiletu;
	}
	public void setCenaBiletu(float cenaBiletu) {
		this.cenaBiletu = cenaBiletu;
	}
	
	
}
