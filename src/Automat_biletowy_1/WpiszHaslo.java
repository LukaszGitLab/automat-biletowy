package Automat_biletowy_1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class WpiszHaslo {
	JFrame ramkaHaslo;
	JLabel etykieta;
//	JTextField hasloField;
	JPasswordField hasloField;
	
	public WpiszHaslo(){
		tworzGUI();
	}	
	
	public void tworzGUI() {
		ramkaHaslo = new JFrame("Wpisz has�o: 123");
		JPanel panelHaslo = new JPanel();
		etykieta = new JLabel();
		etykieta.setText("Wpisz has�o");
		panelHaslo.add(etykieta);			
//		hasloField = new JTextField(20);
		hasloField = new JPasswordField(20);
		panelHaslo.add(hasloField);
		JButton okButton = new JButton("OK");panelHaslo.add(okButton);
		okButton.addActionListener(new SprawdzHaslo());
		
		JButton anulujButton = new JButton("Anuluj");panelHaslo.add(anulujButton); 
		anulujButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ramkaHaslo.dispose();
//				new Automat();
				new PrezentacjaBiletow();
			}
		});
		
		
		ramkaHaslo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		ramkaHaslo.add(panelHaslo);
		ramkaHaslo.setSize(400, 100);
		ramkaHaslo.pack();
		ramkaHaslo.setLocation(0, 500);
		ramkaHaslo.setLocationRelativeTo(null);
		ramkaHaslo.setVisible(true);
	}
	
	class SprawdzHaslo implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(hasloField.getText().equals("123")) {
				ramkaHaslo.dispose();
//				new TworzNowyBilet();
				new AdministratorBiletow();
				System.out.println("Poprawne has�o, " + hasloField.getText());
			}
			else{				
				System.out.println("B��dne has�o, " + hasloField.getText());
				hasloField.setText("B��DNE HAS�O");
				
				hasloField.requestFocusInWindow();
				hasloField.selectAll();
			}
			
		}
		
	}
}
