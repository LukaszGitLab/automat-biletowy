package Automat_biletowy_1;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.TreeSet;

import javax.swing.*;
import javax.swing.border.Border;

import Automat_biletowy_1.Automat.ZalogujJakoAdministrator;

public class PrezentacjaBiletow {
	JFrame ramkaPrezentacja;
	JButton[] listaButtonNormalne = new JButton[Automat.listaBiletow.size()];
	JButton[] listaButtonUlgowe = new JButton[Automat.listaBiletow.size()];
	JLabel iloscWybranychBiletow;
	JLabel cenaWybranychBiletow;
	JFrame ramkaPlusMinus;
	JLabel labelNazwa;
	JFrame ramkaPodsumowanie;
	
	int iloscBiletow = 1;
	ArrayList<Bilet> listaBiletowDoKupienia = new ArrayList<Bilet>();
	JLabel[] listaWybranychBiletow = new JLabel[listaBiletowDoKupienia.size()];
	String[] rB ;
	static ArrayList<Bufor> arrayPodsumowanie = new ArrayList<Bufor>();
	JLabel[] iloscBiletowPodsumowanie;
	JLabel[] cenaBiletowPodsumowanie;
	JLabel cenaRazemPodsumowanie;
	String cenaJednostkowaPodsumowanie;
	static float cenaRazemPodsumowanieStatic = 0.00f;
	TreeSet<Integer> tabTreeSetPrezentacja;// = new TreeSet<Integer>();

	
//	private int r;
//	String rodzajBiletu;
	
	public PrezentacjaBiletow(){
		tworzGUI();
	}

	private void tworzGUI() {
		ramkaPrezentacja = new JFrame("Prezentacja bilet�w, �ukasz K�pka 01.2018");
		ramkaPrezentacja.setLayout(new GridBagLayout());
		JPanel panelA = new JPanel();
//		panelA.setLayout(new BoxLayout(panelA, BoxLayout.Y_AXIS));
		GridLayout experimentLayout = new GridLayout(0,2,4,4);
		panelA.setLayout(experimentLayout);
//		JPasswordField passwd= new JPasswordField("Haslo");	panelA.add(passwd);
		JLabel labelPowitanie = new JLabel("Wybierz bilet: Normalny ");
	Font font = new Font("serif", Font.BOLD, 24);labelPowitanie.setFont(font);
		panelA.add(labelPowitanie);
		JLabel labelUlgowe = new JLabel("lub Ulgowy");
		labelUlgowe.setFont(font);
		panelA.add(labelUlgowe);
		
		for(int i = 0; i < Automat.listaBiletow.size(); i++){
			listaButtonNormalne[i] = new JButton("Normalny " + Automat.listaBiletow.get(i).getNazwaBiletu());
			panelA.add(listaButtonNormalne[i]); 
			listaButtonNormalne[i].setName("Normalny/" + Automat.listaBiletow.get(i).getNazwaBiletu());
			listaButtonNormalne[i].setActionCommand("Normalny/" + i + "/1");
			listaButtonNormalne[i].addActionListener(new PlusMinus());
			
			listaButtonUlgowe[i] = new JButton("Ulgowy " + Automat.listaBiletow.get(i).getNazwaBiletu());
			panelA.add(listaButtonUlgowe[i]);
			listaButtonUlgowe[i].setName("Ulgowy/" +  Automat.listaBiletow.get(i).getNazwaBiletu());
//			listaButtonUlgowe[i].setActionCommand("Ulgowy/" +  i + "/2");
			listaButtonUlgowe[i].setActionCommand("Ulgowy/" +  i + "/" + UstalenieCenyUlgowej.dzielnikCenyUlgowej);
			listaButtonUlgowe[i].addActionListener(new PlusMinus());
		}
		
		JButton przyciskRezygnacja = new JButton("Rezygnacja z zakup�w");	
		przyciskRezygnacja.setForeground(Color.WHITE);
		przyciskRezygnacja.setBackground(Color.BLUE);
		przyciskRezygnacja.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ramkaPrezentacja.dispose();
				arrayPodsumowanie.clear();

				new PrezentacjaBiletow();
			}
		});
		przyciskRezygnacja.setVisible(false);
		panelA.add(przyciskRezygnacja);
		
		if(!arrayPodsumowanie.isEmpty()) {
			przyciskRezygnacja.setVisible(true);
		}
		
		JButton przyciskPodsumowanieZakupow = new JButton("Podsumowanie");
		przyciskPodsumowanieZakupow.setForeground(Color.BLACK);
		przyciskPodsumowanieZakupow.setBackground(Color.GREEN);
		przyciskPodsumowanieZakupow.setActionCommand("1");
		przyciskPodsumowanieZakupow.addActionListener(new Podsumowanie());
		przyciskPodsumowanieZakupow.setVisible(false);
		if(!arrayPodsumowanie.isEmpty()) {
			przyciskPodsumowanieZakupow.setVisible(true);
		}
		panelA.add(przyciskPodsumowanieZakupow);
		
		JButton zalogujJakoAdministrator = new JButton("Opcje dla serwisanta");panelA.add(zalogujJakoAdministrator);
		zalogujJakoAdministrator.setForeground(Color.WHITE);
		zalogujJakoAdministrator.setBackground(Color.BLACK);
		zalogujJakoAdministrator.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ramkaPrezentacja.dispose();
				arrayPodsumowanie.clear();
				new WpiszHaslo();
			}
		});	

		
		
		ramkaPrezentacja.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ramkaPrezentacja.add(panelA);
		ramkaPrezentacja.setSize(400, 600);
		ramkaPrezentacja.pack();
		ramkaPrezentacja.setLocation(800, 0);
		ramkaPrezentacja.setLocationRelativeTo(null);
		ramkaPrezentacja.setVisible(true);	
	}
/////////////////////////////////////////////////////////////////////////////////////
	class PlusMinus implements ActionListener {		
		
		@Override
		public void actionPerformed(ActionEvent e) {

			rB = e.getActionCommand().split("/");
			for(int i = 0; i < rB.length; i++) {
				System.out.println("Bilet " + rB[i]);
				System.out.println("getSorce() = " + e.getSource());
			}
			JButton tmp = (JButton) e.getSource();
			String imieButton = tmp.getText();
			System.out.println("Name = " + imieButton);
			
			ramkaPrezentacja.dispose();
			ramkaPlusMinus = new JFrame("PlusMinus");
//			ramkaPlusMinus.setLayout(new GridBagLayout());
			
			JPanel panelNORTH = new JPanel();
			labelNazwa = new JLabel();panelNORTH.add(labelNazwa);		
			labelNazwa.setText("Bilet: " + rB[0] + " " + Automat.listaBiletow.get(Integer.parseInt(rB[1])).getNazwaBiletu()); 
//			JLabel labelCena = new JLabel("ssssss");panelNORTH.add(labelCena);		
//			labelCena.setText("Bilet " + rodzajBiletu + ": " + Automat.listaBiletow.get(Integer.parseInt(e.getActionCommand())).getCenaBiletu()); 
			
			JPanel panelCENTER = new JPanel();
//			GridLayout experimentLayout = new GridLayout(0,3,4,4);
//			panelCENTER.setLayout(experimentLayout);	
			panelCENTER.setLayout( new GridLayout(0,2,4,4));	
			JButton przyciskPlus = new JButton(" + ");	
			przyciskPlus.addActionListener(new Plus());
			panelCENTER.add(przyciskPlus);
			JButton przyciskMinus = new JButton(" - ");
			przyciskMinus.addActionListener(new Minus());
			panelCENTER.add(przyciskMinus);
			
			JLabel textLiczbaBiletow = new JLabel("Liczba bilet�w");panelCENTER.add(textLiczbaBiletow);
			JLabel textCena = new JLabel("Cena z�");panelCENTER.add(textCena);
			iloscWybranychBiletow = new JLabel("" + iloscBiletow);
			panelCENTER.add(iloscWybranychBiletow);
			
//			cenaWybranychBiletow = new JLabel("" + String.format("%.2f", (Automat.listaBiletow.get(  Integer.parseInt(rB[1])  ).getCenaBiletu())/(Integer.parseInt(rB[2]))));
			cenaWybranychBiletow = new JLabel("" + String.format("%.2f",(float) (Automat.listaBiletow.get(  Integer.parseInt(rB[1])  ).getCenaBiletu())*(Float.parseFloat(rB[2]))));			
			panelCENTER.add(cenaWybranychBiletow);

			cenaJednostkowaPodsumowanie = cenaWybranychBiletow.getText();
						
			JPanel panelSOUTH = new JPanel();
			
			JButton przyciskKontynuujZakupy = new JButton("Kontynuuj zakupy");	
			przyciskKontynuujZakupy.setForeground(Color.BLACK);
			przyciskKontynuujZakupy.setBackground(Color.WHITE);
			przyciskKontynuujZakupy.addActionListener(new KontynuujZakupy());
			panelSOUTH.add(przyciskKontynuujZakupy);
			
			JButton przyciskPodsumowanieZakupow = new JButton("Podsumowanie");
			przyciskPodsumowanieZakupow.setForeground(Color.BLACK);
			przyciskPodsumowanieZakupow.setBackground(Color.GREEN);
			przyciskPodsumowanieZakupow.setActionCommand("2");
			przyciskPodsumowanieZakupow.addActionListener(new Podsumowanie());
			panelSOUTH.add(przyciskPodsumowanieZakupow);
			
			ramkaPlusMinus.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			ramkaPlusMinus.getContentPane().add(BorderLayout.NORTH, panelNORTH);
			ramkaPlusMinus.getContentPane().add(BorderLayout.CENTER, panelCENTER);
			ramkaPlusMinus.getContentPane().add(BorderLayout.SOUTH, panelSOUTH);
			ramkaPlusMinus.add(panelCENTER);
			ramkaPlusMinus.setSize(400, 300);
			ramkaPlusMinus.pack();
			ramkaPlusMinus.setLocation(800, 500);
			ramkaPlusMinus.setLocationRelativeTo(null);
			ramkaPlusMinus.setVisible(true);	
		}	
		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class Plus implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			iloscBiletow = iloscBiletow + 1;
			iloscWybranychBiletow.setText(""+iloscBiletow);
			
			cenaWybranychBiletow.setText(""+ String.format("%.2f",(float) (iloscBiletow) * ((float)(Automat.listaBiletow.get(  Integer.parseInt(rB[1])  ).getCenaBiletu())*(Float.parseFloat(rB[2])))));
//			cenaWybranychBiletow.setText(""+ iloscBiletow * 2 ) ;//Automat.listaBiletow.get(Integer.parseInt(e.getActionCommand())).getCenaBiletu()));
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class Minus implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(iloscBiletow > 0) {
				iloscBiletow = iloscBiletow - 1;	
			}
			iloscWybranychBiletow.setText(""+iloscBiletow);
			cenaWybranychBiletow.setText(""+ String.format("%.2f",(float) (iloscBiletow) * ((float)(Automat.listaBiletow.get(  Integer.parseInt(rB[1])  ).getCenaBiletu())*(Float.parseFloat(rB[2])))));
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class Podsumowanie implements ActionListener{			
		@Override
		public void actionPerformed(ActionEvent e) {										
			if(e.getActionCommand() == "2") // z PlusMinus
			{
				Bufor bufor = new Bufor();
				arrayPodsumowanie.add(bufor);
				ramkaPlusMinus.dispose();
								
				//
			}
			if(e.getActionCommand() == "1") // z ramkaPrezentacja
			{
				ramkaPrezentacja.dispose();	
			}
			dodajPowtarzajaceSiePozycje();
			
			ramkaPodsumowanie = new JFrame("Ramka Podsumowanie");			
	//			ramkaPodsumowanie.setLayout(new GridBagLayout());
			JPanel panelNORTH = new JPanel();
			JPanel panelCENTER = new JPanel();
			JPanel panelSOUTH = new JPanel();
			panelCENTER.setLayout( new GridLayout(0,5,4,4));
			JLabel textRodzajBiletu = new JLabel("Nazwa biletu");panelCENTER.add(textRodzajBiletu);
			JLabel textIloscBiletow = new JLabel("Ilo�� szt.");panelCENTER.add(textIloscBiletow);
			textIloscBiletow.setHorizontalAlignment(JLabel.CENTER);
			JLabel textCenaBiletow = new JLabel("Cena z�");panelCENTER.add(textCenaBiletow);
			JLabel textPlus = new JLabel("");panelCENTER.add(textPlus);
			JLabel textMinus = new JLabel("");panelCENTER.add(textMinus);
	//			JButton plus = new JButton("+");panelCENTER.add(plus);
	//			JButton minus = new JButton("-");panelCENTER.add(minus);
		
			JLabel[] nazwaBiletuPodsumowanie = new JLabel[arrayPodsumowanie.size()];
	//			JTextField[] 
					iloscBiletowPodsumowanie = new JLabel[arrayPodsumowanie.size()];
	//			JTextField[]
					cenaBiletowPodsumowanie = new JLabel[arrayPodsumowanie.size()];
			
			JButton[] plusPodsumowanie = new JButton[arrayPodsumowanie.size()];
			JButton[] minusPodsumowanie = new JButton[arrayPodsumowanie.size()];
			
			System.out.println("arrayPodsumowanie.size() = " + arrayPodsumowanie.size());
			for(int i = 0; i < arrayPodsumowanie.size(); i++) {					
				nazwaBiletuPodsumowanie[i] = new JLabel();
				panelCENTER.add(nazwaBiletuPodsumowanie[i]);nazwaBiletuPodsumowanie[i].setText(""+ arrayPodsumowanie.get(i).getNazwa());
				iloscBiletowPodsumowanie[i] = new JLabel();panelCENTER.add(iloscBiletowPodsumowanie[i]);iloscBiletowPodsumowanie[i].setText(""+ arrayPodsumowanie.get(i).getIlosc());
				iloscBiletowPodsumowanie[i].setHorizontalAlignment(JLabel.CENTER);
				Border border2 = BorderFactory.createLineBorder(Color.RED);
				iloscBiletowPodsumowanie[i].setBorder(border2);
				
				cenaBiletowPodsumowanie[i] = new JLabel();panelCENTER.add(cenaBiletowPodsumowanie[i]);cenaBiletowPodsumowanie[i].setText(""+ String.format("%.2f", arrayPodsumowanie.get(i).getCena()));
				cenaBiletowPodsumowanie[i].setBorder(border2);
				
				plusPodsumowanie[i] = new JButton("+");
				plusPodsumowanie[i].setActionCommand("" + i);
				panelCENTER.add(plusPodsumowanie[i]);plusPodsumowanie[i].addActionListener(new PlusPodsumowanie());	
				
				minusPodsumowanie[i] = new JButton("-");
				minusPodsumowanie[i].setActionCommand("" + i);
				panelCENTER.add(minusPodsumowanie[i]);minusPodsumowanie[i].addActionListener(new MinusPodsumowanie());
				System.out.println("SYSO, ilosc " + arrayPodsumowanie.get(i).getIlosc() + ", cena " + String.format("%.2f", arrayPodsumowanie.get(i).getCena()));			
			}
			
			Font font22 = new Font("serif", Font.BOLD, 22);
			JLabel razemPodsumowanie = new JLabel("RAZEM");panelCENTER.add(razemPodsumowanie);razemPodsumowanie.setFont(font22);
			JLabel pustyJLabelPodsumowanie = new JLabel("DO ZAP�ATY");panelCENTER.add(pustyJLabelPodsumowanie);pustyJLabelPodsumowanie.setFont(font22);
			
			cenaRazemPodsumowanie = new JLabel("" + String.format("%.2f",  cenaRazemPodsumowanie()));panelCENTER.add(cenaRazemPodsumowanie);cenaRazemPodsumowanie.setFont(font22);
			Border border3 = BorderFactory.createLineBorder(Color.BLUE);
			cenaRazemPodsumowanie.setBorder(border3);
			
			try {
				cenaRazemPodsumowanieStatic = NumberFormat.getInstance().parse(cenaRazemPodsumowanie.getText()).floatValue();
			} catch(ParseException ex) {
				ex.printStackTrace();
			}	
			System.out.println("---------------------------FLOAT = " + cenaRazemPodsumowanieStatic);
			
			JButton przyciskKontynuujZakupy = new JButton("Kontynuuj zakupy");	
			przyciskKontynuujZakupy.setForeground(Color.BLACK);
			przyciskKontynuujZakupy.setBackground(Color.WHITE);
			przyciskKontynuujZakupy.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {				
					ramkaPodsumowanie.dispose();
					tabTreeSetPrezentacja = new TreeSet<Integer>();
					for(int j = 0; j < arrayPodsumowanie.size(); j++) {
						if(arrayPodsumowanie.get(j).getIlosc() == 0) {
							tabTreeSetPrezentacja.add(j);
							System.out.println(">   tabTreeSetPrezentacja = " + tabTreeSetPrezentacja);
						}
					}
					
					Iterator<Integer> it = tabTreeSetPrezentacja.descendingIterator();
					while(it.hasNext()) {
//							System.out.print(">  tabTreeSetPrezentacja, it.next() = " + it.next() + ", ;");  UWAGA: it.next() jest r�wnie� wykonywane w syso 
							arrayPodsumowanie.remove((int) it.next());
					}
					new PrezentacjaBiletow();
				}
			});
			panelSOUTH.add(przyciskKontynuujZakupy);
			
			JButton przyciskRezygnacja = new JButton("Rezygnacja z zakup�w");	
			przyciskRezygnacja.setForeground(Color.WHITE);
			przyciskRezygnacja.setBackground(Color.BLACK);
			przyciskRezygnacja.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					ramkaPodsumowanie.dispose();
					arrayPodsumowanie.clear();
			
					new PrezentacjaBiletow();
				}
			});
			panelSOUTH.add(przyciskRezygnacja);
			
			JButton przyciskZaplac = new JButton("Zap�a�");	
			przyciskZaplac.setForeground(Color.BLACK);
			przyciskZaplac.setBackground(Color.GREEN);
			przyciskZaplac.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if(cenaRazemPodsumowanie() > 0) {
						ramkaPodsumowanie.dispose();
						new Zaplac();	
					}
					else {
						ramkaPodsumowanie.dispose();
						arrayPodsumowanie.clear();
						Zaplac.optionsComboWybrane = 0;
						Zaplac.rodzajBiletuWybrane = 0;
						new PrezentacjaBiletow();
					}
				
				}
			});
			panelSOUTH.add(przyciskZaplac);
			
			ramkaPodsumowanie.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			ramkaPodsumowanie.getContentPane().add(BorderLayout.NORTH, panelNORTH);
			ramkaPodsumowanie.getContentPane().add(BorderLayout.CENTER, panelCENTER);			
			ramkaPodsumowanie.getContentPane().add(BorderLayout.SOUTH, panelSOUTH);
	//			ramkaPodsumowanie.add(panelNORTH);
//			ramkaPodsumowanie.setSize(400, 300);
			ramkaPodsumowanie.pack();
			ramkaPodsumowanie.setLocation(0, 0);
			ramkaPodsumowanie.setLocationRelativeTo(null);
			ramkaPodsumowanie.setVisible(true);	
			
			
		}	
	}
/////////////////////////////////////////////////////////////////////////////////////
	public float cenaRazemPodsumowanie() {
		float suma = 0;
		float poz = 0;
		for(int i = 0; i < cenaBiletowPodsumowanie.length; i++) {
		
			try {
				poz = NumberFormat.getInstance().parse(cenaBiletowPodsumowanie[i].getText().toString()).floatValue();
			} catch(ParseException nfe) {
				System.out.println("�le zdefiniowany FLOAT");
				nfe.printStackTrace();
			}
					
			suma = suma + poz;
		}
		return suma;
	}
/////////////////////////////////////////////////////////////////////////////////////
	class KontynuujZakupy implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			arrayPodsumowanie.add(new Bufor());
			ramkaPlusMinus.dispose();
			dodajPowtarzajaceSiePozycje();
			new PrezentacjaBiletow();
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////	
	class PlusPodsumowanie implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
//			System.out.println("lukasz =" + iloscBiletowPodsumowanie[0].getText());
			System.out.println("Przycisk PlusPodsumowanie " + e.getActionCommand());
			int iloscBiletowPodsumowaniePlus = Integer.parseInt(iloscBiletowPodsumowanie[Integer.parseInt(e.getActionCommand())].getText());
			iloscBiletowPodsumowaniePlus++;
					
			arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).setIlosc(iloscBiletowPodsumowaniePlus);
			iloscBiletowPodsumowanie[Integer.parseInt(e.getActionCommand())].setText("" + arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).getIlosc());

			arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).setCena((float) arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).getCenaJedn()
					* iloscBiletowPodsumowaniePlus);
			cenaBiletowPodsumowanie[Integer.parseInt(e.getActionCommand())].setText("" +  String.format("%.2f", (float)(
					arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).getCenaJedn()
					* iloscBiletowPodsumowaniePlus)));
			

			cenaRazemPodsumowanie.setText("" + String.format("%.2f", (float) cenaRazemPodsumowanie()));
			try {
				cenaRazemPodsumowanieStatic = NumberFormat.getInstance().parse(cenaRazemPodsumowanie.getText()).floatValue();
			} catch(ParseException ex) {
				ex.printStackTrace();
			}	
			System.out.println("---------------------------FLOAT = " + cenaRazemPodsumowanieStatic);
			
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class MinusPodsumowanie implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Przycisk MinusPodsumowanie " + e.getActionCommand());
			int iloscBiletowPodsumowaniePlus = Integer.parseInt(iloscBiletowPodsumowanie[Integer.parseInt(e.getActionCommand())].getText());
			if(iloscBiletowPodsumowaniePlus > 0) iloscBiletowPodsumowaniePlus--;
					
			arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).setIlosc(iloscBiletowPodsumowaniePlus);
			iloscBiletowPodsumowanie[Integer.parseInt(e.getActionCommand())].setText("" + arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).getIlosc());

			
			if(iloscBiletowPodsumowaniePlus == 0){
				arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).setCena(0.00f);
				cenaBiletowPodsumowanie[Integer.parseInt(e.getActionCommand())].setText("" + String.format("%.2f", 0.00f));
			}	
			else {
				cenaBiletowPodsumowanie[Integer.parseInt(e.getActionCommand())].setText("" +  String.format("%.2f", (float)(
						arrayPodsumowanie.get(Integer.parseInt(e.getActionCommand())).getCenaJedn()
						* iloscBiletowPodsumowaniePlus)));
			}			

			cenaRazemPodsumowanie.setText("" + String.format("%.2f", (float) cenaRazemPodsumowanie()));
			try {
				cenaRazemPodsumowanieStatic = NumberFormat.getInstance().parse(cenaRazemPodsumowanie.getText()).floatValue();
			} catch(ParseException ex) {
				ex.printStackTrace();
			}	
			System.out.println("---------------------------FLOAT = " + cenaRazemPodsumowanieStatic);
		}		
	}
/////////////////////////////////////////////////////////////////////////////////////
	class Bufor{
		private String nazwa;
		private int ilosc;
		private float cena;
		private float cenaJedn;

		public Bufor() {
			zbierzDane();
		}
		
		public void zbierzDane() {		
			// nazwa
			nazwa = labelNazwa.getText();
			// ilosc
			ilosc = Integer.parseInt(iloscWybranychBiletow.getText());
			// cena
			try {
				cena = NumberFormat.getInstance().parse(cenaWybranychBiletow.getText().toString()).floatValue();
			} catch(ParseException ex) {
				ex.printStackTrace();
			}
//			cena = (float) (Float.parseFloat(iloscWybranychBiletow.getText())) * cenaJedn;
//			// cenaJedn
			try {
				cenaJedn = NumberFormat.getInstance().parse(cenaJednostkowaPodsumowanie).floatValue();
				System.out.println("CENA JEDN FLOAT = " + cenaJedn);
			} catch(ParseException ex) {
				ex.printStackTrace();
			}
		}
		
		public String getNazwa() {
			return nazwa;
		}
		public void setNazwa(String nazwa) {
			this.nazwa = nazwa;
		}
		public int getIlosc() {
			return ilosc;
		}
		public void setIlosc(int ilosc) {
			this.ilosc = ilosc;
		}
		public float getCena() {
			return cena;
		}
		public void setCena(float cena) {
			this.cena = cena;
		}
		public float getCenaJedn() {
			return cenaJedn;
		}
		public void setCenaJedn(float cenaJedn) {
			this.cenaJedn = cenaJedn;
		}
		
	}
/////////////////////////////////////////////////////////////////////////////////////
	public void dodajPowtarzajaceSiePozycje() {
		// HashMap ?
		
		for(int i = 0; i < arrayPodsumowanie.size(); i++){
			for(int j = 0; j < arrayPodsumowanie.size(); j++){
				if(i != j){
					if(Objects.equals(arrayPodsumowanie.get(i).getNazwa() , arrayPodsumowanie.get(j).getNazwa())){
						int k = arrayPodsumowanie.get(i).getIlosc();
						int m = arrayPodsumowanie.get(j).getIlosc();
						arrayPodsumowanie.get(i).setIlosc(k+m);
						float n = arrayPodsumowanie.get(i).getCena();
						float p = arrayPodsumowanie.get(j).getCena();
						arrayPodsumowanie.get(i).setCena(n+p);
						arrayPodsumowanie.remove(j);
						System.out.println(" $ Warto�� i = " + i + ", j = " + j);
						i = 0;
						break;
//						continue;
					}
				} 
			}
			
		}
	}
/////////////////////////////////////////////////////////////////////////////////////
	
/////////////////////////////////////////////////////////////////////////////////////
	
/////////////////////////////////////////////////////////////////////////////////////
}
